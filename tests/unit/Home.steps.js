import { defineFeature, loadFeature } from 'jest-cucumber';
import { mount, createLocalVue } from '@vue/test-utils';

import Home from '@/views/Home.vue';

const feature = loadFeature('tests/features/Home.feature');

defineFeature(feature, test => {
  let localVue;

  beforeEach(() => {
    localVue = createLocalVue();
  });

  test('Пользователь открывает главную страницу', ({ given, then }) => {
    let wrapper;

    given('открывает главную страницу', () => {
      wrapper = mount(Home, { localVue });
    });

    then('видит логотип Vuetify', () => {
      expect(wrapper.find('div[style^="img/logo')).toBeDefined();
    });

    then('видит заголовок "Welcome to Vuetify"', () => {
      expect(wrapper.find('h1').text()).toMatch('Welcome to Vuetify');
    });
  });
});

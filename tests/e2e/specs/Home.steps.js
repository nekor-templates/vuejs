import { Given, Then } from 'cypress-cucumber-preprocessor/steps';

Given('открывает главную страницу', () => {
  cy.visit('/');
});

Then('видит логотип Vuetify', () => {
  cy.get('div[style^="img/logo"]');
});

Then('видит заголовок "Welcome to Vuetify"', () => {
  cy.contains('h1', 'Welcome to Vuetify');
});
